﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace Practica2
{
    public partial class cargaMasiva : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnTipo_Click(object sender, EventArgs e)
        {
            if (csvTipo.HasFile) {

                String nom = System.IO.Path.GetFileName(csvTipo.FileName);
                String ext = System.IO.Path.GetExtension(csvTipo.FileName).ToLower();
                String extension = csvTipo.FileName;
                String urlsave = @"C:\arch\" + extension;
                String urlmdb = @"C:/arch/" + extension;
                csvTipo.SaveAs(urlsave);

                try {
                    if (ext.Contains(".csv"))
                    {
                        Literal1.Text = "<div class=\"alert alert-primary\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                        "Archivo " + nom + " cargado exitosamente en la tabla Tipo" +
                        "</div>";

                        //CONEXION
                        String cCon = "Server= localhost; UserID= root ; Database= practica2_fiestasa; Password= realmadrid12; ";
                        var con = new MySqlConnection(cCon);
                        con.Open();

                        //CONSULTA
                        String consulta = "LOAD DATA INFILE \"" + urlmdb + "\" INTO TABLE tipo FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n';";
                        var cmd = new MySqlCommand(consulta, con);
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        Literal1.Text = "<div class=\"alert alert-danger\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                        "Debe cargar un archivo .CSV" +
                        "</div>";
                    }
                } catch (Exception) {
                    Literal1.Text = "<div class=\"alert alert-danger\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                        "No se pudo realizar la carga del archivo, no cumple con el formato establecido" +
                        "</div>";
                }

                



            }
            else {
                Literal1.Text = "<div class=\"alert alert-danger\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                    "Debe cargar un archivo" +
                    "</div>";
            }
        }

        protected void botonMaterial_Click(object sender, EventArgs e)
        {
            if (csvMaterial.HasFile)
            {

                String nom = System.IO.Path.GetFileName(csvMaterial.FileName);
                String ext = System.IO.Path.GetExtension(csvMaterial.FileName).ToLower();
                String extension = csvMaterial.FileName;
                String urlsave = @"C:\arch\" + extension;
                String urlmdb = @"C:/arch/" + extension;
                csvMaterial.SaveAs(urlsave);

                try
                {
                    if (ext.Contains(".csv"))
                    {
                        Literal1.Text = "<div class=\"alert alert-primary\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                        "Archivo " + nom + " cargado exitosamente en la tabla Material" +
                        "</div>";

                        //CONEXION
                        String cCon = "Server= localhost; UserID= root ; Database= practica2_fiestasa; Password= realmadrid12; ";
                        var con = new MySqlConnection(cCon);
                        con.Open();

                        //CONSULTA
                        String consulta = "LOAD DATA INFILE \"" + urlmdb + "\" INTO TABLE material FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n';";
                        var cmd = new MySqlCommand(consulta, con);
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        Literal1.Text = "<div class=\"alert alert-danger\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                        "Debe cargar un archivo .CSV" +
                        "</div>";
                    }
                }
                catch (Exception)
                {
                    Literal1.Text = "<div class=\"alert alert-danger\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                        "No se pudo realizar la carga del archivo, no cumple con el formato establecido"+
                        "</div>";
                }

            }
            else
            {
                Literal1.Text = "<div class=\"alert alert-danger\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                    "Debe cargar un archivo" +
                    "</div>";
            }
        }

        protected void botonColor_Click(object sender, EventArgs e)
        {

            if (csvColor.HasFile)
            {

                String nom = System.IO.Path.GetFileName(csvColor.FileName);
                String ext = System.IO.Path.GetExtension(csvColor.FileName).ToLower();
                String extension = csvColor.FileName;
                String urlsave = @"C:\arch\" + extension;
                String urlmdb = @"C:/arch/" + extension;
                csvColor.SaveAs(urlsave);

                try
                {
                    if (ext.Contains(".csv"))
                    {
                        Literal1.Text = "<div class=\"alert alert-primary\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                        "Archivo " + nom + " cargado exitosamente en la tabla Color" +
                        "</div>";

                        //CONEXION
                        String cCon = "Server= localhost; UserID= root ; Database= practica2_fiestasa; Password= realmadrid12; ";
                        var con = new MySqlConnection(cCon);
                        con.Open();

                        //CONSULTA
                        String consulta = "LOAD DATA INFILE \"" + urlmdb + "\" INTO TABLE color FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n';";
                        var cmd = new MySqlCommand(consulta, con);
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        Literal1.Text = "<div class=\"alert alert-danger\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                        "Debe cargar un archivo .CSV" +
                        "</div>";
                    }
                }
                catch (Exception)
                {
                    Literal1.Text = "<div class=\"alert alert-danger\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                        "No se pudo realizar la carga del archivo, no cumple con el formato establecido" +
                        "</div>";
                }

            }
            else
            {
                Literal1.Text = "<div class=\"alert alert-danger\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                    "Debe cargar un archivo" +
                    "</div>";
            }

        }

        protected void botonProducto_Click(object sender, EventArgs e)
        {
            if (csvProducto.HasFile)
            {

                String nom = System.IO.Path.GetFileName(csvProducto.FileName);
                String ext = System.IO.Path.GetExtension(csvProducto.FileName).ToLower();
                String extension = csvProducto.FileName;
                String urlsave = @"C:\arch\" + extension;
                String urlmdb = @"C:/arch/" + extension;
                csvProducto.SaveAs(urlsave);

                try
                {
                    if (ext.Contains(".csv"))
                    {
                        Literal1.Text = "<div class=\"alert alert-primary\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                        "Archivo " + nom + " cargado exitosamente en la tabla Producto" +
                        "</div>";

                        //CONEXION
                        String cCon = "Server= localhost; UserID= root ; Database= practica2_fiestasa; Password= realmadrid12; ";
                        var con = new MySqlConnection(cCon);
                        con.Open();

                        //CONSULTA
                        String consulta = "LOAD DATA INFILE \"" + urlmdb + "\" INTO TABLE producto FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n';";
                        var cmd = new MySqlCommand(consulta, con);
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        Literal1.Text = "<div class=\"alert alert-danger\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                        "Debe cargar un archivo .CSV" +
                        "</div>";
                    }
                }
                catch (Exception)
                {
                    Literal1.Text = "<div class=\"alert alert-danger\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                        "No se pudo realizar la carga del archivo, no cumple con el formato establecido" +
                        "</div>";
                }





            }
            else
            {
                Literal1.Text = "<div class=\"alert alert-danger\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                    "Debe cargar un archivo" +
                    "</div>";
            }
        }

        protected void botonUsuario_Click(object sender, EventArgs e)
        {
            if (csvUsuario.HasFile)
            {

                String nom = System.IO.Path.GetFileName(csvUsuario.FileName);
                String ext = System.IO.Path.GetExtension(csvUsuario.FileName).ToLower();
                String extension = csvUsuario.FileName;
                String urlsave = @"C:\arch\" + extension;
                String urlmdb = @"C:/arch/" + extension;
                csvUsuario.SaveAs(urlsave);

                try
                {
                    if (ext.Contains(".csv"))
                    {
                        Literal1.Text = "<div class=\"alert alert-primary\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                        "Archivo " + nom + " cargado exitosamente en la tabla Usuario" +
                        "</div>";

                        //CONEXION
                        String cCon = "Server= localhost; UserID= root ; Database= practica2_fiestasa; Password= realmadrid12; ";
                        var con = new MySqlConnection(cCon);
                        con.Open();

                        //CONSULTA
                        String consulta = "LOAD DATA INFILE \"" + urlmdb + "\" INTO TABLE usuario FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n';";
                        var cmd = new MySqlCommand(consulta, con);
                        cmd.ExecuteNonQuery();
                    }
                    else
                    {
                        Literal1.Text = "<div class=\"alert alert-danger\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                        "Debe cargar un archivo .CSV" +
                        "</div>";
                    }
                }
                catch (Exception)
                {
                    Literal1.Text = "<div class=\"alert alert-danger\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                        "No se pudo realizar la carga del archivo, no cumple con el formato establecido" +
                        "</div>";
                }





            }
            else
            {
                Literal1.Text = "<div class=\"alert alert-danger\" role=\"alert\" style=\"width:50%;margin: 0 auto;\"> " +
                    "Debe cargar un archivo" +
                    "</div>";
            }
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cargaMasivaEmpleado.aspx.cs" Inherits="Practica2.cargaMasivaEmpleado" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
	<title>Productos Fiesta S.A.</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
</head>

<body>

    <nav class="navbar navbar-dark bg-primary navbar-expand-lg">
        <a class="navbar-brand" href="#">
        <img src="/images/usac.png" width="230" height="90" class="d-inline-block align-center" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Carga Masiva<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Productos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.aspx">Cerrar Sesion</a>
                </li>
            </ul>
        </div>
   </nav>

   
    <div>
        <br />
        <br />
        <br />
      <asp:Literal ID="Literal1" runat="server"></asp:Literal>
    </div>
    

   <div class="container " style="display: flex; justify-content: space-between; flex-wrap: wrap;">
        <div class="card" style="width: 13rem;margin-top: 50px; ">
                <img class="card-img-top" src="/images/csv.png" alt="CSV" height="200">
                <div class="card-body">
                    <h5 class="card-title">Tipo</h5>
                    <form id="form1" runat="server">
                    <p class="card-text">Carga un archivo .csv para agregar datos a la tabla Tipo Producto</p>
                    <p class="card-text">
                        
                            <asp:FileUpload ID="csvTipo" runat="server" Font-Size="Small" Width="100%" style="margin:0;" />
                    </p>
                   <asp:Button ID="btnTipo" runat="server" Text="Cargar .CSV" style="margin:0;margin-top:20px" class="btn btn-outline-primary" Width="100%" OnClick="btnTipo_Click" />
                </div>
            </div>
            <div class="card" style="width: 13rem; margin-top: 50px;">
                    <img class="card-img-top" src="/images/mat.png" alt="CSV" height="200">
                    <div class="card-body">
                        <h5 class="card-title">Material</h5>
                            <p class="card-text">Carga un archivo .csv para agregar datos a la tabla Material Producto</p>
                        <p class="card-text">
                            <asp:FileUpload ID="csvMaterial" runat="server"  Font-Size="Small" Width="100%" style="margin:0;"/>
                        </p>                      
                        <asp:Button ID="botonMaterial" runat="server" Text="Cargar .CSV" style="margin:0;margin-top:20px" class="btn btn-outline-primary" Width="100%" OnClick="botonMaterial_Click" />
                    </div>
                </div>
                <div class="card" style="width: 13rem; margin-top: 50px;">
                        <img class="card-img-top" src="/images/paint.png" alt="CSV" height="200">
                        <div class="card-body">
                            <h5 class="card-title">Color</h5>                         
                            <p class="card-text">Carga un archivo .csv para agregar datos a la tabla Color Producto</p>
                            <p class="card-text">
                                <asp:FileUpload ID="csvColor" runat="server"  Font-Size="Small" Width="100%" style="margin:0;"/>
                            </p>
                            <asp:Button ID="botonColor" runat="server" Text="Cargar .CSV" style="margin:0;margin-top:20px" class="btn btn-outline-primary" Width="100%" OnClick="botonColor_Click1" />
                        </div>
                    </div>
                    <div class="card" style="width: 13rem; margin-top: 50px;">
                            <img class="card-img-top" src="/images/prod.png" alt="CSV" height="200">
                            <div class="card-body">
                                <h5 class="card-title">Producto</h5>
                                
                                <p class="card-text">Carga un archivo .csv para agregar datos a la tabla Producto</p>
                                <p class="card-text">
                                    <asp:FileUpload ID="csvProducto" runat="server"  Font-Size="Small" style="margin:0;" Width="100%" />
                                </p>
                                <asp:Button ID="botonProducto" runat="server" Text="Cargar .CSV" style="margin:0;margin-top:20px" class="btn btn-outline-primary" Width="100%" OnClick="botonProducto_Click1" />
                            </div>
                        </div>
                
                      
   </div>

     

   </form>

               
</body>
</html>

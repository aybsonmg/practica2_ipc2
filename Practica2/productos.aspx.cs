﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;

namespace Practica2
{
    public partial class verDatos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                //CONEXION
                String cCon = "Server= localhost; UserID= root ; Database= practica2_fiestasa; Password= realmadrid12; ";
                var con = new MySqlConnection(cCon);
                con.Open();

                //CONSULTA
                String consulta = " SELECT prod.codigo As 'CODIGO', prod.descripcion As 'DESCRIPCION', " +
                    "prod.cantidadDisponible As 'CANTIDAD', prod.costoPorUnidad As 'COSTO', " +
                    "us.nombre As 'NOMBRE USUARIO', us.apellido As 'APELLIDO USUARIO', t.nombre As 'TIPO'," +
                    "m.nombre As 'MATERIAL',c.nombre As 'COLOR' " +
                    "FROM producto prod, usuario us, tipo t, material m, color c WHERE prod.codigoUsuario = us.codigo " +
                    "AND  prod.codigoTipo = t.codigo AND prod.codigoMaterial = m.codigo AND prod.codigoColor = c.codigo;";
                var cmd = new MySqlCommand(consulta, con);
                var reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        LiteralCodigo.Text += "" +
                            "<tr>" +
                            "<th scope=\"row\">"+reader.GetString(0)+"</th>" +
                            "<td>" + reader.GetString(1) + "</td>" +
                            "<td>" + reader.GetString(2) + "</td>" +
                            "<td>" + reader.GetString(3) + "</td>" +
                            "<td>" + reader.GetString(4) + " " + reader.GetString(5)+  "</td>" +
                            "<td>" + reader.GetString(6) + "</td>" +
                            "<td>" + reader.GetString(7) + "</td>" +
                            "<td>" + reader.GetString(8) + "</td>" +
                            "</tr>";

                    }
                }
                else
                {
                   
                }



                con.Close();
            }
            catch (Exception) { }


        }
    }
}